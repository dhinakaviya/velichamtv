package velicham.com.velichamlive;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        final VideoView  video = (VideoView) findViewById(R.id.videoView);
        final ProgressBar load = (ProgressBar)findViewById(R.id.progressBar);



        try {

            MediaController mediacontroller = new MediaController(
                    MainActivity.this);
            mediacontroller.setAnchorView(video);

            String url = "http://edge-ind.inapcdn.in:1935/wrap/smarts4n.stream_aac/playlist.m3u8";
            //String url = "http://104.199.219.140:1935/live/srtv/playlist.m3u8";
            Uri videolink = Uri.parse(url);

            video.setMediaController(null);
            video.setVideoURI(videolink);

        } catch (Exception e) {
           //Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        video.requestFocus();
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mp) {
               // dialog.dismiss();
                load.setVisibility(View.GONE);
                video.start();
            }
        });

    }

}

